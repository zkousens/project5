/*
Main program for the virtual memory project.
Make all of your modifications to this file.
You may add or rearrange any code or data as you need.
The header files page_table.h and disk.h explain
how to use the page table and disk interfaces.
*/

#include "page_table.h"
#include "disk.h"
#include "program.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>

// Define Global Variables
const char* ALGORITHM;
int NPAGES;
int NFRAMES;
int PAGEFAULTS=0;
int ACCESSFAULTS=0;
int DISKREAD=0;
int DISKWRITE=0;
struct disk *DISK;
int FRAMEFULL=0;
int FIRST=0;
int clock_hand = 0;

typedef struct{
    int pagemap;
    int pflag;
    int references;
} frame_struct;

frame_struct* frame_table=NULL;
int* REFTABLE = NULL;
int TCOUNT = 0;

// Function prototypes
int find_frame(struct page_table *pt, int page);
int checkFull(struct page_table *pt);
void resetReftable(struct page_table *pt);


// Prints the data outputs for the program
void summary(){
    printf("Summary:\n---------------\n");
    printf("Total Page Faults: %d\n",PAGEFAULTS);
    printf("\t->Actual Page Faults: %d\n",PAGEFAULTS-ACCESSFAULTS);
    printf("\t->Access Faults: %d\n",ACCESSFAULTS);
    printf("Disk Reads: %d\n",DISKREAD);
    printf("Disk Writes: %d\n",DISKWRITE);
}
// Returns the status of the bits for a page
int checkBits(struct page_table *pt, int page){
    int frameResult = -1;
    int bitResult = -1;
    int *frameResult_p = &frameResult;
    int *bitResult_p = &bitResult;
    page_table_get_entry(pt,page,frameResult_p,bitResult_p);
    return bitResult;
}
// Returns the frame that a page is currently stored in
int getFrame(struct page_table *pt, int page){
    int frameResult = -1;
    int bitResult = -1;
    int *frameResult_p = &frameResult;
    int *bitResult_p = &bitResult;
    page_table_get_entry(pt,page,frameResult_p,bitResult_p);
    return frameResult;
}

// Takes an inputted frame, kicks the page that was in that frame
// to the disk, and replaces it with a new page
void kickframe(struct page_table *pt, int page, int frame){
    char * physmem = page_table_get_physmem(pt);
    // page=page to insert
    // we need to find the page to evict
    int rmpage=frame_table[frame].pagemap;
    // Write the evicted page to the disk
    disk_write(DISK,rmpage,&physmem[frame*PAGE_SIZE]);
    DISKWRITE++;
    // Read the new page from the disk
    disk_read(DISK,page,&physmem[frame*PAGE_SIZE]);
    DISKREAD++;
    // If no bits have been set, set the read bit
    if(checkBits(pt, page) == 0){
      page_table_set_entry(pt,page,frame,PROT_READ);
      page_table_set_entry(pt,rmpage,0,0);
    }
    // If the read bit is set, also set the write bit
    else if(checkBits(pt, page) == 1){
      page_table_set_entry(pt,page,frame,PROT_READ|PROT_WRITE);
      page_table_set_entry(pt,rmpage,0,0);
    }
    // Increment the reference counter, used by the custom algorithm
    REFTABLE[page]++;
}

// This is the random handler. It randomly selects a frame,
// evicts the page in that frame, and replaces it with a new page
void rand_handler(struct page_table *pt, int page, int frame){
    int nframes = page_table_get_nframes(pt);
    const int randFrame = (rand() % (nframes-1)) + 1;
    kickframe(pt,page,randFrame);
    frame_table[randFrame].pagemap = page;
}

// This is the FIFO handler, it evicts the earliest page that was
// stored and replaces it with a new page
void fifo_handler(struct page_table *pt, int page, int frame){
    int nframes = page_table_get_nframes(pt);
    // Use the first set frame if we have to evict
    if(FIRST>=nframes){
	    // If the LRU is bigger than the number of frames we need to reset it to zero
	    FIRST=0;
    }
    //Kick the frame out using the kickframe function
    kickframe(pt,page,FIRST);
    // Set the frame_table pagemap to keep track of which page is in the frame
    frame_table[FIRST].pagemap = page;
    //Increment LRU so the next frame will be kicked upon function call
    FIRST++;
}

// This is a helper function for the custom handler that finds the page
// that has been referenced the least
int getMinRef(struct page_table *pt){
  int npages = page_table_get_npages(pt);
  // Pick a random page to start at
  int start = rand() % (npages-1) + 1;
  int min = REFTABLE[start];
  int minPage = start;
  int i = 0;
  while(i<npages){
    if(REFTABLE[i] <= min){
      min = REFTABLE[i];
      minPage = i;
    }
    i++;
  }
  return minPage;
}


// This is an implementation of the Not Frequently Used algo
// It has a counter that keeps track of the number of times
// each page is referenced. When looking for a page to evict,
// it kicks the page that has been referenced the least
void custom_handler(struct page_table *pt, int page, int frame){
  int npages = page_table_get_npages(pt);
  // Find the least used frame
  int luPage = getMinRef(pt);
  // Find the frame that page resides in
  int frame2kick = getFrame(pt,luPage);
  kickframe(pt,page,frame2kick);
  frame_table[frame2kick].pagemap = page;
  // Reset the refrence counters after every
  if(TCOUNT>1000){
    resetReftable(pt);
    TCOUNT = 0;
  }
  TCOUNT++;
  REFTABLE[luPage] = rand() % (npages-1) + 1;
}

// Finds an open frame
int find_frame(struct page_table *pt, int page){
    int i=0;
    int nframes = page_table_get_nframes(pt);
    while(frame_table[i].pagemap != -1 && i < nframes){
        i++;
    }
    return i;
}

// Check if all the frames are occupied
int checkFull(struct page_table *pt){
    int i = 0;
    int nframes = page_table_get_nframes(pt);
    while(i < nframes){
      if(frame_table[i].pagemap == -1){
        return 0;
      }
      i++;
    }
    return 1;
}

// Cleans out the reference counting array after a certain number of cycles
void resetReftable(struct page_table *pt){
   int npages = page_table_get_npages(pt);
   int i=0;
   for(i=0;i<npages;i++){
      	REFTABLE[i] = 0;
    }
}

// The handler that is called when a page fault occurs
void page_fault_handler(struct page_table *pt, int page)
{
    char * physmem = page_table_get_physmem(pt);
    //If there are an equal number of pages and frames, set them on fault
    if(NPAGES == NFRAMES){
      page_table_set_entry(pt,page,page,PROT_READ|PROT_WRITE);
      PAGEFAULTS++;
      ACCESSFAULTS++;
      return;
    }
    int fault_type;
    fault_type = checkBits(pt,page);
    if(fault_type == 0 && checkFull(pt) == 1){
      fault_type = 2;
    }
    int framenum=0;
    switch (fault_type){
        case 0:
            // No bits set. Choose open frame in frame table. map page to frame.
            // Set read bit on page. Read from same page on disk to the frame
            // that the page maps to
            framenum=find_frame(pt,page);
            //Set mapping
            frame_table[framenum].pagemap=page;
            disk_read(DISK,page,&physmem[page*PAGE_SIZE]);
            DISKREAD++;
            page_table_set_entry(pt,page,framenum,PROT_READ);
	          PAGEFAULTS++;
            break;
        case 1:
            // Read bit set. Set write bit on page. Update data on frame that
            // page maps to
            framenum=getFrame(pt,page);
            page_table_set_entry(pt,page,framenum,PROT_READ|PROT_WRITE);
      	    ACCESSFAULTS++;
	          PAGEFAULTS++;
            break;
        case 2:
            // Memory is full. Choose frame to remove based on replacement algorithm.
            // write data in frame back to appropriate page on disk. write data
            // from new page to frame. update frame to page mapping.
	          if(!strcmp(ALGORITHM,"rand")){
                rand_handler(pt,page,framenum);
            }else if(!strcmp(ALGORITHM,"fifo")){
                fifo_handler(pt,page,framenum);
            }else if(!strcmp(ALGORITHM,"custom")){
                custom_handler(pt,page,framenum);
            }else{
                printf("Unknown algorithm, exiting...\n");
                exit(1);
            }
      	    PAGEFAULTS++;
            break;
    }

}

int main(int argc, char *argv[])
{
  srand(5);
  // Print the usage if wrong input is detected
    if (argc != 5) {
	printf
	    ("use: virtmem <npages> <nframes> <rand|fifo|lru|custom> <sort|scan|focus>\n");
	return 1;
    }

    int npages = atoi(argv[1]);
    NPAGES = npages;
    int nframes = atoi(argv[2]);
    NFRAMES = nframes;
    if(nframes < 3){
      printf("Error: nframes must be greater than 3\n");
      exit(1);
    }
    if(npages < 3){
      printf("Error: npages must be greater than 3\n");
      exit(1);
    }
    //Initialize the Frame Table and Ref counter table
    frame_table=malloc(sizeof(frame_struct)*NFRAMES);
    REFTABLE = malloc(sizeof(int)*npages);
    int i=0;
    for(i=0;i<NFRAMES;i++){
        frame_table[i].pagemap=-1;
      	frame_table[i].references=0;
    }
    for(i=0;i<npages;i++){
     	REFTABLE[i] = 0;
    }
    // Parse the algo that was inputted
    ALGORITHM=argv[3];
    const char *program = argv[4];
    // Open the disk
    DISK = disk_open("myvirtualdisk", npages);
    if (!DISK) {
	fprintf(stderr, "couldn't create virtual disk: %s\n",strerror(errno));
	return 1;
    }

    // Create the page table
    struct page_table *pt =
	page_table_create(npages, nframes, page_fault_handler);
    if (!pt) {
	fprintf(stderr, "couldn't create page table: %s\n",
		strerror(errno));
	return 1;
    }

    char *virtmem = page_table_get_virtmem(pt);
    // Parse the program to run
    if (!strcmp(program, "sort")) {
	sort_program(virtmem, npages * PAGE_SIZE);

    } else if (!strcmp(program, "scan")) {
	scan_program(virtmem, npages * PAGE_SIZE);

    } else if (!strcmp(program, "focus")) {
	focus_program(virtmem, npages * PAGE_SIZE);

    } else {
	fprintf(stderr, "unknown program: %s\n", argv[3]);
	return 1;
    }

    // Free everything
    page_table_delete(pt);
    disk_close(DISK);
    free(frame_table);
    free(REFTABLE);
    summary();
    return 0;
}
