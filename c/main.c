/*
Main program for the virtual memory project.
Make all of your modifications to this file.
You may add or rearrange any code or data as you need.
The header files page_table.h and disk.h explain
how to use the page table and disk interfaces.
*/

#include "page_table.h"
#include "disk.h"
#include "program.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
const char* ALGORITHM;
int NPAGES;
int NFRAMES;

void page_fault_handler(struct page_table *pt, int page)
{
    printf("page fault on page #%d\n", page);
    //If there are an equal number of pages and frames, set them on fault
    if(NPAGES == NFRAMES){
      printf("npages == nframes == %d, resolving using direct mapping\n",NPAGES);
      page_table_set_entry(pt,page,page,PROT_READ|PROT_WRITE);
      return;
    }
    //Error Type 1: There is no mapping to physical memory
    //Error Type 2: Pages must e loaded ito memory
    //Error Type 3: Page has incorrect permissions, needs to add permissions
    if(!strcmp(ALGORITHM,"rand")){
        printf("Using rand to handle page fault...\n");
        rand_handler(pt,page);
    }
    else if(!strcmp(ALGORITHM,"fifo")){
        printf("Using fifo to handle page fault...\n");
        fifo_handler(pt,page);
    }
    else if(!strcmp(ALGORITHM,"custom")){
        printf("Using custom to handle page fault...\n");
	custom_handler(pt,page);
    }
    else{
        printf("Unknown algorithm, exiting...\n");
        exit(1);
    }
    exit(1);
}

void rand_handler(struct page_table *pt, int page){   
    
}

void fifo_handler(struct page_table *pt, int page){
}

void custom_handler(struct page_table *pt, int page){
}

int main(int argc, char *argv[])
{
    if (argc != 5) {
	printf
	    ("use: virtmem <npages> <nframes> <rand|fifo|lru|custom> <sort|scan|focus>\n");
	return 1;
    }

    int npages = atoi(argv[1]);
    NPAGES = npages;
    int nframes = atoi(argv[2]);
    NFRAMES = nframes;
    ALGORITHM=argv[3];
    const char *program = argv[4];

    struct disk *disk = disk_open("myvirtualdisk", npages);
    if (!disk) {
	fprintf(stderr, "couldn't create virtual disk: %s\n",strerror(errno));
	return 1;
    }


    struct page_table *pt =
	page_table_create(npages, nframes, page_fault_handler);
    if (!pt) {
	fprintf(stderr, "couldn't create page table: %s\n",
		strerror(errno));
	return 1;
    }

    char *virtmem = page_table_get_virtmem(pt);

    char *physmem = page_table_get_physmem(pt);

    if (!strcmp(program, "sort")) {
	sort_program(virtmem, npages * PAGE_SIZE);

    } else if (!strcmp(program, "scan")) {
	scan_program(virtmem, npages * PAGE_SIZE);

    } else if (!strcmp(program, "focus")) {
	focus_program(virtmem, npages * PAGE_SIZE);

    } else {
	fprintf(stderr, "unknown program: %s\n", argv[3]);
	return 1;
    }

    page_table_delete(pt);
    disk_close(disk);

    return 0;
}
